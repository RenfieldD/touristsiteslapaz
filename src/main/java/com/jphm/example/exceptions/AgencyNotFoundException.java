package com.jphm.example.exceptions;

public class AgencyNotFoundException extends ApplicationException {
    public AgencyNotFoundException(String message) {
        super(message);
    }
}
