package com.jphm.example.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;

import java.time.LocalDateTime;

@ControllerAdvice
public class DefaultExceptionHandler {

    @ExceptionHandler(ApplicationException.class)
    public ResponseEntity<ErrorOutput> handleApplicationException(ApplicationException source, WebRequest request) {
        ErrorOutput errorOutput = new ErrorOutput(
                source.getMessage(),
                HttpStatus.BAD_REQUEST.toString(),
                ((ServletWebRequest) request).getRequest().getRequestURI(),
                LocalDateTime.now().toString());

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorOutput);
    }
}
