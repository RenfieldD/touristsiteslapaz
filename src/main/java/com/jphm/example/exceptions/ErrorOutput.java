package com.jphm.example.exceptions;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class ErrorOutput {

    private String message;

    private String statusCode;

    private String url;

    private String timeStamp;
}
