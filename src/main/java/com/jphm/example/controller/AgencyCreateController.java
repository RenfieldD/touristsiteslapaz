package com.jphm.example.controller;

import com.jphm.example.dto.input.AgencyCreateInput;
import com.jphm.example.dto.input.CustomerCreateInput;
import com.jphm.example.dto.output.CustomerOutput;
import com.jphm.example.factory.AgencyCreateFactory;
import com.jphm.example.factory.CustomerCreateFactory;
import com.jphm.example.model.builder.CustomerOutputBuilder;
import com.jphm.example.model.domain.Agency;
import com.jphm.example.model.domain.Customer;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.annotation.RequestScope;

@Api(
        tags = Constants.AgencyTag.NAME,
        description = Constants.AgencyTag.DESCRIPTION
)
@RestController
@RequestScope
@RequestMapping(value = Constants.BasePath.SECURE_AGENCY)
public class AgencyCreateController {

    @Autowired
    private AgencyCreateFactory factory;

    @ApiOperation(
            value = "Create Agency"
    )
    @PostMapping
    public Agency createAgency(@RequestHeader("userId") Long userId,
                               @RequestBody AgencyCreateInput input) {
        factory.setInput(input);
        factory.execute();

        return factory.getAgency();

        //return CustomerOutputBuilder.getInstance(customer).build();
    }
}
