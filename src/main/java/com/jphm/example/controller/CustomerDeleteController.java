package com.jphm.example.controller;

import com.jphm.example.factory.CustomerDeleteFactory;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.annotation.RequestScope;

@Api(
        tags = Constants.CustomerTag.NAME,
        description = Constants.CustomerTag.DESCRIPTION
)
@RestController
@RequestScope
@RequestMapping(value = Constants.BasePath.SECURE_CUSTOMER)
public class CustomerDeleteController {

    @Autowired
    private CustomerDeleteFactory factory;

    @ApiOperation(
            value = "Delete Customer"
    )
    @DeleteMapping("/{customerId}")
    public void deleteCustomer(@RequestHeader("userId") Long userId,
                               @PathVariable("customerId") Long customerId) {
        factory.setUserId(userId);
        factory.setCustomerId(customerId);
        factory.execute();
    }
}
