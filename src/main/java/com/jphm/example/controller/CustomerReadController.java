package com.jphm.example.controller;

import com.jphm.example.dto.output.CustomerOutput;
import com.jphm.example.factory.CustomerReadFactory;
import com.jphm.example.model.builder.CustomerOutputBuilder;
import com.jphm.example.model.domain.Customer;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.annotation.RequestScope;

@Api(
        tags = Constants.CustomerTag.NAME,
        description = Constants.CustomerTag.DESCRIPTION
)
@RestController
@RequestScope
@RequestMapping(value = Constants.BasePath.SECURE_CUSTOMER)
public class CustomerReadController {

    @Autowired
    private CustomerReadFactory factory;

    @ApiOperation(
            value = "Read Customer"
    )
    @GetMapping("/{customerId}")
    public CustomerOutput readCustomer(@RequestHeader("userId") Long userId,
                                       @PathVariable("customerId") Long customerId) {
        factory.setUserId(userId);
        factory.setCustomerId(customerId);
        factory.execute();

        Customer customer = factory.getCustomer();

        return CustomerOutputBuilder.getInstance(customer).build();
    }
}
