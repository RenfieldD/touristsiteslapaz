package com.jphm.example.controller;

import com.jphm.example.dto.input.CustomerCreateInput;
import com.jphm.example.dto.output.CustomerOutput;
import com.jphm.example.factory.CustomerCreateFactory;
import com.jphm.example.model.builder.CustomerOutputBuilder;
import com.jphm.example.model.domain.Customer;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.annotation.RequestScope;

@Api(
        tags = Constants.CustomerTag.NAME,
        description = Constants.CustomerTag.DESCRIPTION
)
@RestController
@RequestScope
@RequestMapping(value = Constants.BasePath.SECURE_CUSTOMER)
public class CustomerCreateController {

    @Autowired
    private CustomerCreateFactory factory;

    @ApiOperation(
            value = "Create Customer"
    )
    @PostMapping
    public CustomerOutput createCustomer(@RequestHeader("userId") Long userId,
                                         @RequestBody CustomerCreateInput input) {
        factory.setInput(input);
        factory.execute();

        Customer customer = factory.getCustomer();

        return CustomerOutputBuilder.getInstance(customer).build();
    }
}
