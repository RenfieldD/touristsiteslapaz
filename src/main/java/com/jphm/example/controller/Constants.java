package com.jphm.example.controller;

public final class Constants {

    public static class BasePath {

        public static final String SECURE = "/secure";

        public static final String SYSTEM = "/system";

        public static final String PUBLIC = "/public";

        public static final String SECURE_CUSTOMER = SECURE + "/customers";

        public static final String SECURE_AGENCY = SECURE + "/agencies";
    }

    public static final class CustomerTag {
        public static final String NAME = "Customer Controller";

        public static final String DESCRIPTION = "Operations over Customer";
    }

    public static final class AgencyTag {
        public static final String NAME = "Agency Controller";

        public static final String DESCRIPTION = "Operations over Agency";
    }
}
