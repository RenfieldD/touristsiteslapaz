package com.jphm.example.controller;

import com.jphm.example.factory.CustomerGeneratePdf;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.annotation.RequestScope;

@RestController
@RequestScope
@RequestMapping(value = "/customers")
public class CustomerGeneratePdfController {

    @Autowired
    private CustomerGeneratePdf factory;

    @GetMapping("/{customerId}")
    public HttpEntity<byte[]> customerPdf(@RequestHeader("userId") Long userId,
                                          @PathVariable("customerId") Long customerId) {
        factory.setUserId(userId);
        factory.setCustomerId(customerId);
        factory.execute();

        return new HttpEntity<>(factory.getBaos().toByteArray(), factory.getHeaders());
    }
}
