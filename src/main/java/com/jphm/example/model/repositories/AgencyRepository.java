package com.jphm.example.model.repositories;

import com.jphm.example.model.domain.Agency;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface AgencyRepository extends JpaRepository<Agency, Long> {

    @Override
    @Query("SELECT item FROM Agency item WHERE item.id = :id AND item.isDeleted = false")
    Optional<Agency> findById(@Param("id") Long id);
}
