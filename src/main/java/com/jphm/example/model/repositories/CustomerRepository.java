package com.jphm.example.model.repositories;

import com.jphm.example.model.domain.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface CustomerRepository extends JpaRepository<Customer, Long> {
    @Override
    @Query(value = "SELECT item FROM Customer item WHERE item.id = :id AND item.isDeleted = FALSE")
    Optional<Customer> findById(@Param("id") Long id);
}
