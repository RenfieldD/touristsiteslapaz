package com.jphm.example.model.builder;

import com.jphm.example.dto.output.CustomerOutput;
import com.jphm.example.model.domain.Customer;

public final class CustomerOutputBuilder {

    private final CustomerOutput instance;

    public static CustomerOutputBuilder getInstance(Customer customer) {
        return (new CustomerOutputBuilder()).setCustomer(customer);
    }

    private CustomerOutputBuilder() {
        instance = new CustomerOutput();
    }

    private CustomerOutputBuilder setCustomer(Customer customer) {
        instance.setId(customer.getId());
        instance.setCode(customer.getCode());
        instance.setName(customer.getName());
        instance.setLastName(customer.getLastName());
        instance.setDocumentType(customer.getDocumentType().name());
        instance.setDocumentNumber(customer.getDocumentNumber());
        instance.setEmail(customer.getEmail());
        instance.setPhoneNumber(customer.getPhoneNumber());
        instance.setCustomerState(customer.getCustomerState().name());

        return this;
    }

    public CustomerOutput build() {
        return instance;
    }
}
