package com.jphm.example.model.domain;

public enum AgencyState {
    ACTIVATED,
    DEACTIVATED
}
