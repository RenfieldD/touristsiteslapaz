package com.jphm.example.model.domain;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Type;

import javax.persistence.*;

import java.time.LocalDateTime;

import static com.jphm.example.model.domain.Constants.CustomerTable;

@Getter
@Setter
@Entity
@Table(name = CustomerTable.NAME)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Customer {

    @Id
    @Column(name = CustomerTable.Id.NAME, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = CustomerTable.Code.NAME, nullable = false, length = CustomerTable.Code.LENGTH)
    private String code;

    @Column(name = CustomerTable.Name.NAME, length = CustomerTable.Name.LENGTH)
    private String name;

    @Column(name = CustomerTable.LastName.NAME, length = CustomerTable.LastName.LENGTH)
    private String lastName;

    @Column(name = CustomerTable.DocumentType.NAME, nullable = false, length = CustomerTable.DocumentType.LENGTH)
    @Enumerated(EnumType.STRING)
    private DocumentType documentType;

    @Column(name = CustomerTable.DocumentNumber.NAME, nullable = false, length = CustomerTable.DocumentNumber.LENGTH)
    private String documentNumber;

    @Column(name = CustomerTable.Email.NAME, length = CustomerTable.Email.LENGTH)
    private String email;

    @Column(name = CustomerTable.PhoneNumber.NAME, length = CustomerTable.PhoneNumber.LENGTH)
    private String phoneNumber;

    @Column(name = CustomerTable.CodeQr.NAME, length = CustomerTable.CodeQr.LENGTH)
    private String codeQr;

    @JsonBackReference
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = CustomerTable.AgencyId.NAME, referencedColumnName = Constants.AgencyTable.Id.NAME)
    private Agency agency;

    @Column(name = CustomerTable.CustomerState.NAME, nullable = false, length = CustomerTable.CustomerState.LENGTH)
    @Enumerated(EnumType.STRING)
    private CustomerState customerState;

    @Type(type = "org.hibernate.type.NumericBooleanType")
    @Column(name = CustomerTable.IsDeleted.NAME, nullable = false)
    private Boolean isDeleted;

    @Column(name = CustomerTable.CreatedDate.NAME, nullable = false, updatable = false)
    private LocalDateTime createdDate;

    @PrePersist
    void onPrePersist() {
        this.createdDate = LocalDateTime.now();
        this.customerState = CustomerState.ACTIVATED;
        this.isDeleted = Boolean.FALSE;
    }
}
