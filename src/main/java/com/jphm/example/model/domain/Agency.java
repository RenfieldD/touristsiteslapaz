package com.jphm.example.model.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Type;

import javax.persistence.*;

import java.time.LocalDateTime;

import static com.jphm.example.model.domain.Constants.AgencyTable;

@Setter
@Getter
@Entity
@Table(name = Constants.AgencyTable.NAME)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Agency {

    @Id
    @Column(name = AgencyTable.Id.NAME, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = AgencyTable.Name.NAME, nullable = false, length = AgencyTable.Name.LENGTH)
    private String name;

    @Column(name = AgencyTable.Address.NAME, nullable = false, length = AgencyTable.Address.LENGTH)
    private String address;

    @Column(name = AgencyTable.Logo.NAME, length = AgencyTable.Logo.LENGTH)
    private String logo;

    @Column(name = AgencyTable.Phone.NAME, length = AgencyTable.Phone.LENGTH)
    private String phone;

    @Column(name = AgencyTable.AgencyState.NAME, nullable = false, length = AgencyTable.AgencyState.LENGTH)
    @Enumerated(EnumType.STRING)
    private AgencyState agencyState;

    @Type(type = "org.hibernate.type.NumericBooleanType")
    @Column(name = AgencyTable.IsDeleted.NAME, nullable = false)
    private Boolean isDeleted;

    @Column(name = AgencyTable.CreatedDate.NAME, nullable = false, updatable = false)
    private LocalDateTime createdDate;

    @PrePersist
    void prePersist() {
        this.agencyState = AgencyState.ACTIVATED;
        this.isDeleted = false;
        this.createdDate = LocalDateTime.now();
    }
}
