package com.jphm.example.model.domain;

public final class Constants {

    public static class CustomerTable {
        public static final String NAME = "customer";

        public static class Id {
            public static final String NAME = "id";
        }

        public static class Code {
            public static final String NAME = "code";

            public static final int LENGTH = 30;
        }

        public static class Name {
            public static final String NAME = "name";

            public static final int LENGTH = 30;
        }

        public static class LastName {
            public static final String NAME = "last_name";

            public static final int LENGTH = 30;
        }

        public static class DocumentType {
            public static final String NAME = "document_type";

            public static final int LENGTH = 10;
        }

        public static class DocumentNumber {
            public static final String NAME = "document_number";

            public static final int LENGTH = 20;
        }

        public static class Email {
            public static final String NAME = "email";

            public static final int LENGTH = 50;
        }

        public static class PhoneNumber {
            public static final String NAME = "phone_number";

            public static final int LENGTH = 30;
        }

        public static class CodeQr {
            public static final String NAME = "code_qr";

            public static final int LENGTH = 800;
        }

        public static class AgencyId {
            public static final String NAME = "agency_id";
        }

        public static class CustomerState {
            public static final String NAME = "customer_state";

            public static final int LENGTH = 20;
        }

        public static class IsDeleted {
            public static final String NAME = "is_deleted";
        }

        public static class CreatedDate {
            public static final String NAME = "created_date";
        }
    }

    public static class AgencyTable {
        public static final String NAME = "agency";

        public static class Id {
            public static final String NAME =  "id";
        }

        public static class Name {
            public static final String NAME = "name";

            public static final int LENGTH = 50;
        }

        public static class Address {
            public static final String NAME = "address";

            public static final int LENGTH = 100;
        }

        public static class Logo {
            public static final String NAME = "logo";

            public static final int LENGTH = 250;
        }

        public static class Phone {
            public static final String NAME = "phone";

            public static final int LENGTH = 30;
        }

        public static class AgencyState {
            public static final String NAME = "agency_state";

            public static final int LENGTH = 20;
        }

        public static class IsDeleted {
            public static final String NAME = "is_deleted";
        }

        public static class CreatedDate {
            public static final String NAME = "created_date";
        }
    }
}
