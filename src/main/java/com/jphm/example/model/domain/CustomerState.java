package com.jphm.example.model.domain;

public enum CustomerState {
    ACTIVATED,
    DEACTIVATED
}
