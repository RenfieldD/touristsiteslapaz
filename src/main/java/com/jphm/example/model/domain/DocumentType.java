package com.jphm.example.model.domain;

public enum DocumentType {
    CI,
    PASSPORT,
    LSM,
    RUAT
}
