package com.jphm.example.factory;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.lowagie.text.pdf.parser.Matrix;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;
import org.thymeleaf.templateresolver.FileTemplateResolver;
import org.xhtmlrenderer.pdf.ITextRenderer;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Base64;


@Scope(value = "prototype")
@Component
@Slf4j
public class HtmlFactory {

    public String convertTemplateToHtml(Object data, String templateName) {
        SpringTemplateEngine templateEngine = new SpringTemplateEngine();
        FileTemplateResolver templateResolver = new FileTemplateResolver();
        templateResolver.setPrefix("src/main/resources/templates/");
        templateResolver.setCacheable(false);
        templateResolver.setSuffix(".html");
        templateResolver.setTemplateMode("HTML");
        templateResolver.setForceTemplateMode(true);
        templateEngine.setTemplateResolver(templateResolver);

        Context ctx = new Context();
        ctx.setVariable("customer", data);
        log.info("Customer: ");
        log.info(templateEngine.process(templateName, ctx));

        return templateEngine.process(templateName, ctx);
    }

    public HttpHeaders generateHeaderPdf(String name) {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Expires", "0");
        httpHeaders.add("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
        httpHeaders.add("Pragma", "public");
        httpHeaders.add("Content-Disposition", "inline; filename=" + name + ".pdf");
        httpHeaders.setContentType(MediaType.parseMediaType("application/pdf"));

        return httpHeaders;
    }

    public ByteArrayOutputStream generatePdf(String html) {
        Document document = Jsoup.parse(html, "UTF-8");
        document.outputSettings().syntax(Document.OutputSettings.Syntax.xml);

        try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
            ITextRenderer renderer = new ITextRenderer();
            renderer.setDocumentFromString(document.html());
            renderer.layout();
            renderer.createPDF(baos);
            return baos;

        } catch (IOException e) {
            throw new IllegalArgumentException("Ocurrio un problema al generar el pdf");
        }
    }

    public String generateQrCode(String txt) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        QRCodeWriter barcodeWriter = new QRCodeWriter();
        BitMatrix bitMatrix;

        try {
            bitMatrix = barcodeWriter.encode(txt, BarcodeFormat.QR_CODE, 200, 200);
            BufferedImage bufferedImage = MatrixToImageWriter.toBufferedImage(bitMatrix);
            ImageIO.write(bufferedImage, "png", outputStream);

        }catch (WriterException | IOException e) {
            throw new IllegalArgumentException("Ocurrio un problema al generar el qr code");
        }

        return "data:image/png;base64," + new String(Base64.getEncoder().encode(outputStream.toByteArray()));
    }
}
