package com.jphm.example.factory;

import com.jphm.example.exceptions.CustomerNotFoundException;
import com.jphm.example.model.domain.Customer;
import com.jphm.example.model.repositories.CustomerRepository;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

import java.io.ByteArrayOutputStream;

@Scope(value = "prototype")
@Component
public class CustomerGeneratePdf {

    @Setter
    private Long userId;

    @Setter
    private Long customerId;

    @Getter
    private ByteArrayOutputStream baos;

    @Getter
    private HttpHeaders headers;

    @Autowired
    private HtmlFactory htmlFactory;

    @Autowired
    private CustomerRepository customerRepository;

    public void execute() {
        Customer foundCustomer = findCustomer(customerId);
        foundCustomer.setCodeQr(generateCodeQr("CHMJ-123456"));
        String html = htmlFactory.convertTemplateToHtml(foundCustomer, "customer");
        baos = htmlFactory.generatePdf(html);
        headers = htmlFactory.generateHeaderPdf("abc-123");
    }

    private Customer findCustomer(Long customerId) {
        return customerRepository.findById(customerId)
                .orElseThrow(() -> new CustomerNotFoundException("No se encontro al cliente con id: " + customerId));
    }

    private String generateCodeQr(String text) {
        return htmlFactory.generateQrCode(text);
    }
}
