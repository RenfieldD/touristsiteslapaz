package com.jphm.example.factory;

import com.jphm.example.dto.input.AgencyCreateInput;
import com.jphm.example.model.domain.Agency;
import com.jphm.example.model.repositories.AgencyRepository;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Scope(value = "prototype")
@Component
public class AgencyCreateFactory {

    @Setter
    private Long userId;

    @Setter
    private AgencyCreateInput input;

    @Getter
    private Agency agency;

    @Autowired
    private AgencyRepository repository;

    public void execute() {
        agency = repository.save(composeAgency(input));
    }

    private Agency composeAgency(AgencyCreateInput input) {
        Agency instance = new Agency();
        instance.setName(input.getName());
        instance.setAddress(input.getAddress());
        instance.setLogo(input.getLogo());
        instance.setPhone(input.getPhone());

        return instance;
    }
}
