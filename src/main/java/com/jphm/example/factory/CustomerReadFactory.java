package com.jphm.example.factory;

import com.jphm.example.exceptions.CustomerNotFoundException;
import com.jphm.example.model.domain.Customer;
import com.jphm.example.model.repositories.CustomerRepository;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Scope(value = "prototype")
@Component
public class CustomerReadFactory {

    @Setter
    private Long userId;

    @Setter
    private Long customerId;

    @Getter
    private Customer customer;

    @Autowired
    private CustomerRepository repository;

    public void execute() {
        customer = findCustomer(customerId);
    }

    private Customer findCustomer(Long customerId) {
        return repository.findById(customerId)
                .orElseThrow(() -> new CustomerNotFoundException("Cliente no encontrado por el id: " + customerId));
    }
}
