package com.jphm.example.factory;

import com.jphm.example.dto.input.CustomerCreateInput;
import com.jphm.example.exceptions.AgencyNotFoundException;
import com.jphm.example.model.domain.Agency;
import com.jphm.example.model.domain.Customer;
import com.jphm.example.model.domain.DocumentType;
import com.jphm.example.model.repositories.AgencyRepository;
import com.jphm.example.model.repositories.CustomerRepository;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Scope(value = "prototype")
@Component
public class CustomerCreateFactory {

    @Setter
    private CustomerCreateInput input;

    @Getter
    private Customer customer;

    @Autowired
    private CustomerRepository repository;

    @Autowired
    private AgencyRepository agencyRepository;

    public void execute() {
        Agency agency = findAgency(input.getAgencyId());
        Customer newCustomer = composeCustomer(input, agency);
        customer = repository.save(newCustomer);
    }

    private Agency findAgency(Long agencyId) {
        return agencyRepository.findById(agencyId)
                .orElseThrow(() -> new AgencyNotFoundException("No se encontro la agencia con id: " + agencyId));
    }

    private Customer composeCustomer(CustomerCreateInput input, Agency agency) {
        Customer customer = new Customer();
        customer.setCode(input.getCode());
        customer.setName(input.getName());
        customer.setLastName(input.getLastName());
        customer.setDocumentType(DocumentType.valueOf(input.getDocumentType()));
        customer.setDocumentNumber(input.getDocumentNumber());
        customer.setEmail(input.getEmail());
        customer.setPhoneNumber(input.getPhoneNumber());
        customer.setAgency(agency);

        return customer;
    }
}
