package com.jphm.example.dto.input;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AgencyCreateInput {

    private String name;

    private String address;

    private String logo;

    private String phone;
}
