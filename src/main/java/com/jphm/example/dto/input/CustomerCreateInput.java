package com.jphm.example.dto.input;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class CustomerCreateInput {

    private String code;

    private String name;

    private String lastName;

    private String documentType;

    private String documentNumber;

    private String email;

    private String phoneNumber;

    private Long agencyId;
}
