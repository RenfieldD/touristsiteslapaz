package com.jphm.example.dto.output;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CustomerOutput {

    private Long id;

    private String code;

    private String name;

    private String lastName;

    private String documentType;

    private String documentNumber;

    private String email;

    private String phoneNumber;

    private String customerState;
}
