package com.jphm.example.dao;

import com.jphm.example.model.domain.Customer;

public interface CustomerDao {

    Customer findById(Long id);


}
